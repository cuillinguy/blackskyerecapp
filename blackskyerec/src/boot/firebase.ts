// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from 'firebase/app';

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import 'firebase/analytics';

// Add the Firebase products that you want to use
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';


const firebaseConfig = {		
    apiKey: 'AIzaSyCWFFjIOYy8WHcLoio3Yd4qIOiX9IWPkSw',
    authDomain: 'blackskyerecapp.firebaseapp.com',
    databaseURL: 'https://blackskyerecapp.firebaseio.com',
    projectId: 'blackskyerecapp',
    storageBucket: 'blackskyerecapp.appspot.com',
    messagingSenderId: '412626299235',
    appId: '1:412626299235:web:a7d5507d56f4ca18b3cb56',
    measurementId: 'G-Z9LLLQEMC2'	
    };		
    // Initialize Firebase		
    const firebaseApp = firebase.initializeApp(firebaseConfig);


    // Enable offline support
    //https://dev.to/paco_ita/break-the-cache-api-limits-in-our-pwa-oo3
    firebase.firestore().enablePersistence()
    .catch(function(err) {
        
        if(err !== null) {
            if(err.code == 'unimplemented') {
                // The current browser does not support all of the
                // features required to enable persistence
            }
        }
    });
    


    const firebaseAuth = firebaseApp.auth();
    const firebaseDb = firebaseApp.database();
    const firestoreDb = firebase.firestore();
    const firebaseRoot = firebase;


    firebase.analytics();

    export { firebaseAuth, firebaseDb, firestoreDb, firebaseRoot }