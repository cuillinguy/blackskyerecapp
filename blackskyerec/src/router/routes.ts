import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/LandingPage.vue') },
      { path: '/landingpage', component: () => import('pages/LandingPage.vue') },
      { path: '/story', component: () => import('pages/Story.vue') },
      { path: '/:lang/staff', component: () => import('pages/Staff.vue') },
      { path: '/:lang/contactus', component: () => import('pages/ContactUs.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
