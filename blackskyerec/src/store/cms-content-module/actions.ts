import StoryblokClient from 'storyblok-js-client'
const globalToken = 'HqjYQMrLdAyJgiqo1tGnWAtt'
const actions = {

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    loadHome ({ commit }: any, context: { version: any }) {
        const storyapi = new StoryblokClient({
            accessToken: globalToken
          }) 

        console.log('firing loadhome action')
        const slug = 'home'
        return storyapi.get(`cdn/stories/${slug}`, {
          version: context.version
        }).then((res) => {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          console.log('home ' + res.data.story)
          commit('setHome', res.data.story)
        })
    }

}
export default actions