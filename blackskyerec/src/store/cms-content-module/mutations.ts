import { MutationTree } from 'vuex'
import { StateInterface } from './state'

const mutation: MutationTree<StateInterface> = {
    setHome (state, home) {
      state.home = home
    }
}
export default mutation