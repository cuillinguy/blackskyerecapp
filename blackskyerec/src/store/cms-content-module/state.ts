export interface StateInterface {
    cacheVersion: '';
    language: 'en';
    settings: {
      // eslint-disable-next-line camelcase
      main_navi: [];
    };
      prop: boolean;
      home: {
        story: {
          content: {
              body: [],
              chapters: []
              }
          }
      }
    }
    
    const state: StateInterface = {
    cacheVersion: '',
    language: 'en',
    settings: {
      // eslint-disable-next-line camelcase
      main_navi: []
    },
      prop: false,
      home: {
        story: {
        content: {
            body: [],
            chapters: []
            }
        }
      },
    }
    
    export default state