import { ActionTree } from 'vuex';
import { StoreInterface } from '../index';
import { FireStoreStateInterface } from './state';
import { firestoreDb } from '../../boot/firebase'
import { firestoreAction } from 'vuexfire'

const actions: ActionTree<FireStoreStateInterface, StoreInterface> = {
  someAction (/* context */) {
    // your code
  },
  getFireStoreUsers () : void {
    // Get users from Firestore
    try {
      const db = firestoreDb
        .collection('users')
      // const internalCollection = []
          db.onSnapshot((userRef) => {
              userRef.forEach((doc) => {
                  console.log('id from module: ' + doc.id)
              })
          })
    } catch (error) {
      
    }
  },
  setUsersRef: firestoreAction(({ bindFirestoreRef }) => {
    const db = firestoreDb
    bindFirestoreRef('users', db.collection('users'))
  })
};

export default actions;
