import { GetterTree } from 'vuex';
import { StoreInterface } from '../index';
import state, { FireStoreStateInterface } from './state';

const getters: GetterTree<FireStoreStateInterface, StoreInterface> = {
  someAction (/* context */) {
    // your code
  },
  users () {
    return state.users
  }
};

export default getters;
