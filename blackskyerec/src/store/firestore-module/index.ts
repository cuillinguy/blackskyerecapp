import { Module } from 'vuex';
import { StoreInterface } from '../index';
import state, { FireStoreStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const firestoreModule: Module<FireStoreStateInterface, StoreInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};

export default firestoreModule;
