import { MutationTree } from 'vuex';
import { FireStoreStateInterface } from './state';

const mutation: MutationTree<FireStoreStateInterface> = {
  someMutation (/* state: ExampleStateInterface */) {
    // your code
  }
};

export default mutation;
