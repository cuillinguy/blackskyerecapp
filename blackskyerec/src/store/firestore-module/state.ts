export interface FireStoreStateInterface {
  prop: boolean;
  users: []
}

const state: FireStoreStateInterface = {
  prop: false,
  users: []
};

export default state;
