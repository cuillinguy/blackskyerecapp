import { store } from 'quasar/wrappers';
import Vuex from 'vuex';

// import example from './module-example';
// import { ExampleStateInterface } from './module-example/state';
import firestoreModule from './firestore-module'
import nav, { NAV_MODULE } from './nav-module'
import content, { CONTENT_MODULE } from './cms-content-module'
import createPersistedState from 'vuex-persistedstate'
import { vuexfireMutations } from 'vuexfire'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export interface StoreInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  example: unknown;
}

export default store(function ({ Vue }) {
  Vue.use(Vuex);

  const Store = new Vuex.Store<StoreInterface>({
    modules: {
      [NAV_MODULE]: nav,
      [CONTENT_MODULE]: content,
      firestoreModule
    },
    plugins: [createPersistedState()],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV,
    mutations: {
      ...vuexfireMutations
    }
  });

  return Store;
});
