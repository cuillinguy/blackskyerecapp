import { MutationTree } from 'vuex'
import { StateInterface } from './state'

const mutation: MutationTree<StateInterface> = {
    setSettings (state, settings) {
      state.settings = settings
    },
    setLanguage (state, language) {
      state.language = language
    },
    setCacheVersion (state, version) {
      state.cacheVersion = version
    }
}
export default mutation